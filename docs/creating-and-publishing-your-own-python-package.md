
### 시작하기 전에 ...
이 포스팅에서는 *공개* Python Package Index에 패키지를 게시하는 방법을 자세히 설명한다. 즉, 일단 게시되면 누구나 패키지를 사용할 수 있다. 이후 비공개 PyPI를 설정하는 방법에 대한 포스팅을 할 예정이다.

이 포스팅의 코드 예에서 길을 잃었다면 [여기](https://github.com/mike-huls/mikes-toolbox2)에서 소스 코드를 확인하세요.

## 1. Python 패키지 프로젝트 설정
이 섹션에서는 폴더를 만들고 가상 환경과 패키지를 설치하여 프로젝트를 준비하겠다.

먼저 `~/MyProjects/Exercises/Python/my_packages/new_package`에 **폴더를 생성**하고 코드 편집기에서 이 폴더를 연다. 다음으로 **가상 환경을 설정**해야 한다. 이 작업을 수행하는 방법에 대한 자세한 내용은 [Virtual environments for absolute beginners — what is it and how to create one (+ examples)](https://towardsdatascience.com/virtual-environments-for-absolute-beginners-what-is-it-and-how-to-create-one-examples-a48da8982d4b?source=post_page-----7656c893f387--------------------------------)를 참조하세요. 간단히 말해, PyCharm이 처리하도록 하거나 `python -m venv venv`를 사용할 수 있다.

끝으로 Poetry를 설치해야 한다. 이 패키지를 사용하면 의존성 관리와 패키징이 매우 쉬워진다. `pip install poetry`로 설치한다.

## 2. 패키지 요구사항
Python 패키지가 유효한 패키지가 되려면 특정 **파일과 폴더**가 있어야 하므로 이를 만들겠다. 폴더가 다음과 같은 지 확인한다.

```
c/
└── my_packages/
    └── new_package/                <-- Project dir
        ├── src/                    <-- this is the source dir for our package
        │   ├── mikes_toolbox2/     <-- the package we're building
        │   │   └── __init__.py     <-- empty file
        │   └── __init__.py         <-- empty file
        ├── tests/                  <-- tests go here
        │  └── __init__.py          <-- empty file
        └── README.md               <-- required readme for our project
```

> **Note**:<br>
> 기술적으로는 `new_package`와 `mikes_toolbox2` 사이에 `src` 폴더가 필요하지 않지만, 패키지 코드를 소스 디렉토리에서 조금 더 분리할 수 있기 때문에 그렇게 하는 것을 선호한다. 이 구조가 더 명확하고 깔끔하며 관리하기 쉽다고 생각하지만, 선택 사항이므로 `src` 폴더를 생략해도 된다.

파일과 폴더를 생성한 후에는 **`pyproject.toml`**이 필요하다. 이것은 빌드 시스템, 패키지 관리자 및 린터 같은 모든 종류의 도구에서 사용하도록 설계된 Python 프로젝트를 위한 범용 구성 파일이다. Poetry에서는 종속성을 추적하기 위해 `pyproject.toml`을 사용합니다.

`poetry init`으로 `pyproject.toml` 생성을 도와주기 때문에 Poetry로 `pyproject.toml`을 생성하는 것은 쉽다. 프롬프트에 응답하기만 하면 pyproject가 생성된다.

![](./images/1_zpa7yh9pg-TZlW_TN0iucw.webp)

`pyproject.toml`은 프로젝트 디렉터리의 루트에 위치해야 한다. 이 경우에는 `~/MyProjects/Exercises/Python/my_packages/new_pacakge/pyproject.toml`에 있다.

프로젝트나 패키지에 종속성이 필요할 수 있는데, 어떻게 설치해야 할까? Poetry에서는 무엇보다도 종속성을 추적하기 위해 `pyproject.toml`을 사용한다. `request`과 `black`이라는 두 패키지를 설치하여 설명하겠다. 이 패키지는 조금 다르게 사용된다.

- 패키지 자체는 `request`를 사용한다.
- 패키지를 lint하는 데 우리 개발자는 `black`을 사용한다.

차이점은 `black`은 **dev dependency**이므로 종속성 자체를 개발하는 동안에만 필요하다는 것이다. 따라서 누군가 pip를 통해 패키지를 설치하면 `requests`도 함께 설치해야 하지만, `black`은 그렇지 않다.

다음은 다양한 방법으로 패키지를 설치하는 방법이다.

```bash
$ poetry add requests             <-- will install regularly
$ poetry add black --group dev    <-- will install as a dev dependency
```

같은 방법으로 개발 종속성으로 pytest를 설치한다. 마지막에는 다음과 같은 종속성 그룹을 `pyproject.toml`에 포함한다.

![](./images/1_ztT2j9rjV5nSzQ15wn0d6g.webp)

## 3. 패키지 콘텐츠와 테스팅
함수들을 패키징하자! `~/MyProjects/Exercises/Python/my_pacakges/new_pacakge/src/mikes_toolbox2/my_toolbox.py`에 아래 `mess_up_casing` 함수를 추가한다.

```python
import random


def mess_up_casing(my_string: str) -> str:
    """Messes up the casing of a string completely"""
    return "".join(
        [l.upper() if (round(random.random()) == 1) else l.lower() for l in my_string]
    )
```

이제 이 코드를 테스트해 보겠다. 이를 위해 `tests` 폴더에 [작은 단위 테스트](https://github.com/mike-huls/mikes-toolbox2/blob/main/tests/test_toolbox_functions.py)를 작성했다. `pytest`를 호출하고 모든 테스트가 성공했는지 확인한다. 마지막으로 모든 코드를 `black src`로 포맷할 수도 있다.

## 4. 패키지 빌드와 게시
이제 poetry가 바르게 설정되었으므로 패키지 빌드가 매우 쉬워졌다 `poetry build`.

빌드가 성공하면 `.tar.gz`와 `.whl` 파일이 있는 새 `dist` 폴더를 볼 수 있을 것이다. 이 파일들이 바로 pypi에 업로드할 파일들이다.

이렇게 하려면 먼저 [pypi.org](https://pypi.org/account/register/)에 **등록**해야 한다. 로그인한 후 계정 설정으로 이동하여 **API 토큰**을 생성할 수 있다. 패키지를 게시할 때 인증 시 토큰이 필요하다. 창을 닫은 후에는 다시 볼 수 없으므로 토큰을 어딘가에 저장해야 한다.

다음으로 패키지를 게시하는 데 사용할 **Poetry 구성**해야 한다. Poetry는 pypi.org 계정에 인증하는 방법을 알고 있어야 한다. 이를 위해 이전에 생성한 **API 토큰**을 사용한다 `poetry config pypi-token.pypi THETOKEN`.

이제 프로젝트 구조가 바르게 잡혔고 Poetry를 구성했으니 **패키지 게시**하는 것은 쉽다. `poetry publish`만 호출하면 끝이다! 이 단계가 끝나면 `pip install mikes-toolbox2`를 수해하여 설치할 수 있다.

## 마치며
이 포스팅에서는 Poetry를 사용하면 코드를 매우 쉽게 패키징하고 PyPI.org에 게시할 수 있다는 것을 살펴보았다. 코드를 패키징하는 데는 관심이 있지만 공개 PyPI에 게시하고 싶지 않으신가요? 자신만의 Python 패키지 색인을 호스팅하는 것도 가능하다. 
