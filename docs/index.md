# 찐 초보자를 위한 나만의 Python 패키지 만들기와 게시하기 <sup>[1](#footnote_1)</sup>

**5분 안에 Python 패키지를 생성하고 빌드하여 게시하기**

Python 패키지는 여러 프로젝트에서 쉽게 공유하고 구현할 수 있는 재사용 가능한 코드 모음이다. 코드를 한 번 작성하면 여러 곳에서 여러 번 사용할 수 있다. 패키지를 사용하면 동료 또는 전 세계 개발자 커뮤니티와 코드를 공유할 수 있다. 데이터 과학자는 Jupyter 노트북을 공유하는 대신 패키지를 공유하여 쉽게 업데이트하고 재사용하며 버전 관리를 할 수 있다.

이 포스팅에서는 자신만의 패키지를 만들고, 빌드하고, Python Package Index(PyPI; pip 설치 위치)에 게시하는 최신 접근 방식에 대해 자세히 설명한다. ["**mikes-toolbox2**"라는 실제 패키지](https://pypi.org/project/mikes-toolbox2/)를 만들고 PyPI에 배포하여 mikes-toolbox2를 pip으로 설치해 보겠다. 코딩을 시작하자!


<a name="footnote_1">1</a>: 이 페이지는 [Creating and Publishing Your Own Python Package for Absolute Beginners](https://towardsdatascience.com/creating-and-publishing-your-own-python-package-for-absolute-beginners-7656c893f387)를 편역한 것임.
